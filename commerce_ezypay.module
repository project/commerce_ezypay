<?php

/**
 * @file
 * Implements NAB payment services for use in Drupal Commerce.
 */

define('COMMERCE_NAB_TRANSACT_AUTH_CAPTURE', 0);
define('COMMERCE_NAB_TRANSACT_AUTH_ONLY', 11);

/**
 * Implements hook_menu().
 */
function commerce_ezypay_menu() {
  $items = array();

  // Add a menu item for capturing authorizations.
  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/ezypay-capture'] = array(
    'title' => 'EzyPay',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_ezypay_capture_form', 3, 5),
    'access callback' => 'commerce_ezypay_capture_access',
    'access arguments' => array(3, 5),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 2,
    'file' => 'includes/commerce_ezypay.admin.inc',
  );

  return $items;
}

/**
 * Determines access to the prior authorization capture form for NAB
 *   credit card transactions.
 *
 * @param $order
 *   The order the transaction is on.
 * @param $transaction
 *   The payment transaction object to be captured.
 *
 * @return
 *   TRUE or FALSE indicating capture access.
 */
function commerce_ezypay_capture_access($order, $transaction) {
  // Return FALSE if the transaction isn't for ezypay or isn't
  // awaiting capture.
  if ($transaction->payment_method != 'ezypay' || strtoupper($transaction->remote_status) != 'AUTH_ONLY') {
    return FALSE;
  }

  // Return FALSE if it is more than 30 days past the original authorization.
  if (time() - $transaction->created > 86400 * 30) {
    return FALSE;
  }

  // Allow access if the user can update payments on this order.
  return commerce_payment_transaction_access('update', $order, $transaction);
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_ezypay_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['ezypay_credit'] = array(
    'base' => 'commerce_ezypay',
    'title' => t('EzyPay Web service - Credit Card'),
    'short_title' => t('Ezypay'),
    'display_title' => t('Credit card via Ezypay gateway'),
    'description' => t('Adds functionality to process transactions with the EzyPay API'),
  );
  
  $payment_methods['ezypay_debit'] = array(
    'base' => 'commerce_ezypay',
    'title' => t('EzyPay Web service - Direct Debit'),
    'short_title' => t('Ezypay'),
    'display_title' => t('Direct Debit via Ezypay gateway'),
    'description' => t('Adds functionality to process transactions with the EzyPay API'),
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function commerce_ezypay_settings_form($settings = NULL) {
  $form = array();
  
  // Add some defaults.
  $settings = (array)$settings + array(
    'commerce_ezypay_principal_id' => '',
    'commerce_ezypay_principal_dist_acct' => '',
    'commerce_ezypay_debug' => FALSE
  );

  $form['commerce_ezypay_principal_id'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Principal Id'),
    '#description' => t('Enter your ezypay principal id.'),
    '#default_value' => $settings['commerce_ezypay_principal_id'],
  );
  $form['commerce_ezypay_principal_dist_acct'] = array(
    '#type' => 'textfield',
    '#title' => t('Distribution account'),
    '#description' => t('Enter your ezypay distribution account number.'),
    '#default_value' => $settings['commerce_ezypay_principal_dist_acct'],
  );
  $form['commerce_ezypay_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug Mode'),
    '#description' => t('Store debug information in watchdog - <strong>Note this is <em>not</em> PCI Compliant</strong> - credit card numbers are stored unecrypted and hence is only for development environments.'),
    '#default_value' => $settings['commerce_ezypay_debug'],
  );

  return $form;
}

/**
 * Payment method callback: checkout form.
 */
function commerce_ezypay_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $method = $payment_method['method_id'];
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  $form = commerce_payment_credit_card_form(array('owner' => ''));
  
  switch ($method) {
    case 'ezypay_credit':
      
      // Add our extra fields as required by EzyPay
      $ezypay_card_types = array(
        3 => 'Visa',
        4 => 'Master card',
        6 => 'American express',
        7 => 'Diners club'
      );
    
      // Ezypay requires a card type
      $form['credit_card']['card_type'] = array(
        '#type' => 'select',
        '#title' => t('Card Type'),
        // default to Visa
        '#default_value' => isset($pane_values['credit_card']['card_type']) ? $pane_values['credit_card']['card_type'] : 3,
        '#options' => $ezypay_card_types,
      );
      break;
    
    case 'ezypay_debit':
      
      $form['credit_card']['card_type'] = array(
        '#value' => 0, // Bank account
        '#type' => 'value'
      );
      
      // Account name
      $form['credit_card']['owner']['#title'] = t('Account Name');
      
      // No need for these
      unset($form['credit_card']['exp_year']);
      unset($form['credit_card']['exp_month']);

      // Account Bsb      
      $form['credit_card']['bsb'] = array(
        '#type' => 'textfield',
        '#max_length' => 7,
        '#title' => t('Account BSB'),
        '#required' => TRUE,
        '#size' => 10,
        '#default_value' => isset($pane_values['credit_card']['bsb']) ? $pane_values['credit_card']['bsb'] : '',
        '#description' => t('Enter BSB in XXX-XXX format')
      );
      
      // Account Number
      $form['credit_card']['number']['#title'] = t('Account Number');
      
      break;
    
  }
  
  $form['ezypay'] = array(
    '#tree' => TRUE
  );
  
  // Ezypay requires DOB
  $form['ezypay']['dob'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'd-m-Y',
    '#date_year_range' => '-90:-17',
    '#title' => t('Date of Birth'),
    '#description' => t('In order to use the EzyPay Billing on Demand service you must be at least 18 years of age.'),
    '#default_value' => isset($pane_values['ezypay']['dob']) ? $pane_values['ezypay']['dob'] : NULL,
  );
  
  // Ezypay requires agree with T&C
  $form['ezypay']['tnc'] = array(
    '#type' => 'checkbox',
    '#title' => t('I agree with the EzyPay Billing on Demand <a target="_blank" href="!url">Terms and Conditions</a>',
                  array('!url' => url(drupal_get_path('module', 'commerce_ezypay') .'/ezypay-terms-and-conditions.pdf'))),
  );
  
  return $form;
}

/**
 * Payment method callback: checkout form validation.
 */
function commerce_ezypay_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  $method = $payment_method['method_id'];
  $valid = TRUE;
  if ($method == 'ezypay_credit') {
    module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  
    // Validate the credit card fields.
    $settings = array(
      'form_parents' => array_merge($form_parents, array('credit_card')),
    );
  
    if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
      $valid = FALSE;
    }
  }
  else {
    // Validate the account number/bsb/name
    if (drupal_strlen($pane_values['credit_card']['bsb']) < 7 || !preg_match('@[0-9]{3}-[0-9]{3}@', $pane_values['credit_card']['bsb'])) {
      drupal_set_message(t('Your BSB number must be entered in XXX-XXX format.'), 'error');
      $valid = FALSE;
    }
  }
  
  // Validate our extra EzyPay fields
  if (empty($pane_values['ezypay']['tnc'])) {
    drupal_set_message(t('You must agree to the EzyPay terms and conditions.'), 'error');
    $valid = FALSE;
  }
  $dob = $pane_values['ezypay']['dob'];
  $age = commerce_ezypay_age($dob, FALSE);
  if ($age < 18) {
    drupal_set_message(t('You must be at least 18 years of age to use the EzyPay service.'), 'error');
    $valid = FALSE;
  }
  return $valid;
}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_ezypay_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  
  // load our includes
  module_load_include('inc', 'commerce_ezypay', 'includes/commerce_ezypay.soap');
  $response = commerce_ezypay_soap_transaction($payment_method, $order, $pane_values, $charge);
  
  // Prepare a transaction object to log the API response.
  $transaction = commerce_payment_transaction_new('ezypay', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  // We don't actually perform the transaction now - we just authorize it
  $transaction->amount = 0;
  $transaction->currency_code = $charge['currency_code'];
  $transaction->payload[REQUEST_TIME] = $response;
  
  $response_text = array();
  // Check to make sure the response parses and new customer passed properly.
  if (!empty($response->errors) && is_array($response->errors)
      && count($response->errors) > 0) {
    // transaction failed
    foreach ($response->errors as $error) {
      $response_text[] = $error->message;
    }
    
    // Create a failed transaction with the error message.
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  }
  elseif (!empty($response->newCustomerId)) {
    // transaction succeeded
    $transaction->remote_id = $response->newCustomerId;
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $response_text = array(t('Successfully registered a new customer account on EzyPay.'));
  }
  else {
    // Something unexpected happened here no errors but no newCustomerId, so we
    // supply some default values.
    if (empty($response->request_not_made)) {
      $response_text = array(t('Failed to receive a response.'));
      // Log the trouble string to the watchdog.
      watchdog('commerce_ezypay', 'Failed to receive a response.', array(), WATCHDOG_ERROR);
    }
    else {
      $response_text = array(t('Failed to make the request.'));
      // Log the trouble string to the watchdog.
      watchdog('commerce_ezypay', 'Failed to make the request.', array(), WATCHDOG_ERROR);
    }
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  }
  // Store the type of transaction in the remote status.
  $transaction->remote_status = $transaction->status;
  $transaction->message = implode("\n", $response_text);

  // Save the transaction information.
  commerce_payment_transaction_save($transaction);
  
  // If the payment failed, display an error and rebuild the form.
  if ($transaction->status == COMMERCE_PAYMENT_STATUS_FAILURE) {
    if ($payment_method['method_id'] == 'ezypay_credit') {
      drupal_set_message(t('We received the following error processing your card. Please enter you information again or try a different card.'), 'error');
    }
    else {
      drupal_set_message(t('We received the following error processing your account. Please enter you information again or try a different account.'), 'error');      
    }
    foreach ($response_text as $response_message) {
      drupal_set_message(check_plain($response_message), 'error');
    }
    form_set_error(' ', t('Please try again.'));
    return FALSE;
  }
  // Now we associate the customer profile with the user
  $profile = FALSE;
  $billing_pane_id = 'customer_profile_billing';
  if (($field_name = variable_get('commerce_' . $billing_pane_id . '_field', '')) &&
      !empty($order_wrapper->{$field_name})) {
    $profile = $order_wrapper->{$field_name}->value();
  }
  else {
    // Or try the association stored in the order's data array if no field is set.
    if (!empty($order->data['profiles'][$billing_pane_id])) {
      $profile = commerce_customer_profile_load($order->data['profiles'][$billing_pane_id]);
    }
  }
  if ($profile && !empty($profile->profile_id)) {
    $profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', $profile);
    $address = $profile_wrapper->commerce_customer_address->value();
    $record = array(
      'billing_profile_id' => $profile->profile_id,
      'customer_number' => $response->newCustomerId
    );
    drupal_write_record('commerce_ezypay_customer_number', $record);
  }
  else {
    drupal_set_message(t('We were unable to associate your account with your EzyPay customer number, the transaction has been cancelled.'), 'error');
    return FALSE;
  }
}

/**
 * Util function to return age from date field value
 * @param $dob array with year/month/day members
 * @param $parse bool
 *  TRUE to parse array into datestring
*/
function commerce_ezypay_age($dob, $parse = TRUE) {
  if ($parse) {
    $dob = "{$dob['year']}-{$dob['month']}-{$dob['day']}";  
  }
  $dob_obj = new DateTime($dob);
  $age = date('Y') - $dob_obj->format('Y');
  if (date('n') > $dob_obj->format('n')) {
    // Not yet their birth month
    $age--;
  }
  if (date('n') == $dob_obj->format('n') &&
      date('j') < $dob_obj->format('j')) {
    // Not yet their birth day
    $age--;
  }
  return $age;
}