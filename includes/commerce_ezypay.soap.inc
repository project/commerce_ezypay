<?php
/*
 * @file commerce_ezypay.soap.inc
 * Defines the soap classes needed for EzyPay
 * @copyright Copyright(c) 2011 Rowlands Group
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlandsgroup dot com
 * 
 */

/**
 * Provides 
*/
function commerce_ezypay_soap_transaction($payment_method, $order, $pane_values, $charge) {
  $settings = $payment_method['settings'];
  $client = new SoapClient('https://secure.ezypay.com.au/ezypay/service/CustomerService.cfc?wsdl', array(
      "trace"      => 1,
      "exceptions" => 0));
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $address = $profile = FALSE;
  $billing_pane_id = 'customer_profile_billing';
  if ($field_name = variable_get('commerce_' . $billing_pane_id . '_field', '')) {
    $profile = $wrapper->{$field_name}->value();
  }
  else {
    // Or try the association stored in the order's data array if no field is set.
    if (!empty($order->data['profiles'][$billing_pane_id])) {
      $profile = commerce_customer_profile_load($order->data['profiles'][$billing_pane_id]);
    }
  }
  if ($profile) {
    $profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', $profile);
    $address = $profile_wrapper->commerce_customer_address->value();
  }
  if (!$address) {
    return (object) array(
      'request_not_made' => TRUE,
      'errors' => array(
        (object) array(
          'message' => t('You must provide an address in order to complete checkout with Ezypay')
        )
      )
    );
  }
  
  // Build our new customer object
  $newCustomer = array(
    'principalId' => $settings['commerce_ezypay_principal_id'],
    'principalRefNo' => 1,
    'distaccid' => $settings['commerce_ezypay_principal_dist_acct'],
    'confirmTC' => $pane_values['ezypay']['tnc'],
    'firstName' => $address['first_name'],
    'lastName' => $address['last_name'],
    'dob' => commerce_ezypay_to_xsd_date($pane_values['ezypay']['dob']),
    'address1_3' =>  $address['thoroughfare'] . $address['premise'],
    'suburb' => $address['locality'],
    'postcode' => $address['postal_code'],
    'email' => $order->mail,
    'email_confirm' => $order->mail,
    'payPlanId' => '0',
    'amount' => 0,
    'firstAmount' => '0.00',
    'addDate' => commerce_ezypay_to_xsd_date(),
    'nextDate' => commerce_ezypay_to_xsd_date(),
    'debitTypeId' => 4,
    'debitMul' => "1",
    'selpaymethod' => 'paydetlcc',
    'payMethId' => $pane_values['credit_card']['card_type']
  );
  if ($payment_method['method_id'] == 'ezypay_credit') {
    $newCustomer += array(
      'ccName' => $pane_values['credit_card']['owner'],
      'ccNumber' => $pane_values['credit_card']['number'],
      'ccExpiryDate' => commerce_ezypay_to_xsd_date(array('year' => $pane_values['credit_card']['exp_year'],
                                                          'month' => $pane_values['credit_card']['exp_month'],
                                                          'day' => 01), TRUE)
    );
  }
  else {
    // Debit
    $newCustomer += array(
      'accountName' => $pane_values['credit_card']['owner'],
      'accountNumber' => $pane_values['credit_card']['number'],
      'accountBSB' => $pane_values['credit_card']['bsb']
    );
  }
  try {
    $response = $client->addCustomer($newCustomer);
  }
  catch(SoapFault $error) {
    return (object) array(
      'errors' => array(
        (object) array(
          'message' => $e->faultString
        )
      )
    );
  }
  if ($settings['commerce_ezypay_debug']) {
    watchdog('commerce_ezypay', '<pre>' . $client->__getLastRequest() . '</pre>', array(), WATCHDOG_DEBUG);
    watchdog('commerce_ezypay', '<pre>' . $client->__getLastResponse() . '</pre>', array(), WATCHDOG_DEBUG);
  }
  return $response;
}

/**
 * Returns a date in xsd:date format
 *
 * @param $date string/array
 *   the date to be formatted in xsd format
 *   pass NULL for current date
 *   
 * @param $array bool
 *   TRUE if date is an array with year/month/day keys
 *
 * @return string date formatted in xsd:date format
*/
function commerce_ezypay_to_xsd_date($date = NULL, $array = FALSE) {
  if (!$date) {
    return date('c');
  }
  if ($array) {
    $date_obj = new DateTime("{$date['year']}-{$date['month']}-{$date['day']}");
  }
  else {
    $date_obj = new DateTime($date);
  }
  return $date_obj->format('c');
}